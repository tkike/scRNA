---
title: "control01_cas9"
author: "Reka_Toth"
date: '`r format(Sys.time(), "%d %B, %Y")`'
output:
 # html_notebook: default
  workflowr::wflow_html:
    toc: false
    code_folding: "hide"
#output: workflowr::wflow_html:
editor_options:
  chunk_output_type: console
---

## Reading in the result of cellranger processing for the sample control01-cas9

```{r message=FALSE, warning=FALSE}

library(Seurat)
library(dplyr)

###########libraries and functions#############
if (grepl("Windows", Sys.getenv("OS"))){
  PATH ="V:/"} else {
    PATH ="/C010-projects/"}
if (grepl("Windows", Sys.getenv("OS"))){
  PATH_Y="N:/"} else {
    PATH_Y="/C010-datasets/"}
PATH_ICGC <- "/icgc/"


DATA = paste0(PATH, "Reka/33_CoO_lung/scRNASeq/data")
RESULT = "./output/"
CODE = "./code/"
CALLS = paste0(PATH_Y, "External/Sotillo_mouse_lung_cell/scRNASeq_CoO_lung/cellranger_output_30_07/")


```

```{r message=FALSE, warning=FALSE}
sc.data <- Read10X(data.dir = file.path(CALLS, "control01-cas9/filtered_feature_bc_matrix/"))
control01_cas9 <- CreateSeuratObject(counts = sc.data, project = "control01_cas9", min.cells = 3, min.features = 200)


control01_cas9[["percent.mt"]] <- PercentageFeatureSet(control01_cas9, pattern = "^mt")

```

# QC visualization

```{r message=FALSE, warning=FALSE}
VlnPlot(control01_cas9, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)

cat("Number of cells before QC: ", length(Seurat::Cells(control01_cas9)), "\n")
plot1 <- FeatureScatter(control01_cas9, feature1 = "nCount_RNA", feature2 = "percent.mt")
plot2 <- FeatureScatter(control01_cas9, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
plot1 
plot2

control01_cas9 <- subset(control01_cas9, subset = nFeature_RNA > 1000 & nFeature_RNA < 6000 & percent.mt < 10)

cat("Number of cells after QC: ", length(Seurat::Cells(control01_cas9)), "\n")
VlnPlot(control01_cas9, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)


```

# Normalization
```{r message=FALSE, warning=FALSE}
control01_cas9 <- NormalizeData(control01_cas9, normalization.method = "LogNormalize", scale.factor = 10000)

```

```{r message=FALSE, warning=FALSE}
control01_cas9 <- FindVariableFeatures(control01_cas9, selection.method = "vst", nfeatures = 2000)

# Identify the 10 most highly variable genes
top10 <- head(VariableFeatures(control01_cas9), 10)

# plot variable features with and without labels
plot1 <- VariableFeaturePlot(control01_cas9)
plot2 <- LabelPoints(plot = plot1, points = top10, repel = TRUE)
plot1
plot2

```

```{r message=FALSE, warning=FALSE}
#scaling is not recommended...
all.genes <- rownames(control01_cas9)
control01_cas9 <- ScaleData(control01_cas9, features = all.genes)

control01_cas9 <- RunPCA(control01_cas9, features = VariableFeatures(object = control01_cas9))
VizDimLoadings(control01_cas9, dims = 1:2, reduction = "pca")
DimPlot(control01_cas9, reduction = "pca")

DimHeatmap(control01_cas9, dims = 1:15, cells = 500, balanced = TRUE)

#control01_cas9 <- JackStraw(control01_cas9, num.replicate = 100)
#control01_cas9 <- ScoreJackStraw(control01_cas9, dims = 1:20)
#JackStrawPlot(control01_cas9, dims = 1:20)
ElbowPlot(control01_cas9)

```

# Cluster the samples

```{r message=FALSE, warning=FALSE}
control01_cas9 <- FindNeighbors(control01_cas9, dims = 1:50)
control01_cas9 <- FindClusters(control01_cas9, resolution = 0.5)


control01_cas9 <- RunUMAP(control01_cas9, dims = 1:50)
DimPlot(control01_cas9, reduction = "umap")

```

# Find markers

```{r message=FALSE, warning=FALSE, eval=F}
cluster4.markers <- FindMarkers(control01_cas9, ident.1 = 4, min.pct = 0.25)
head(cluster4.markers, n = 5)
cluster3.markers <- FindMarkers(control01_cas9, ident.1 = 3, min.pct = 0.25)
head(cluster3.markers, n = 5)
cluster2.markers <- FindMarkers(control01_cas9, ident.1 = 2, min.pct = 0.25)
head(cluster2.markers, n = 5)
cluster1.markers <- FindMarkers(control01_cas9, ident.1 = 1, min.pct = 0.25)
head(cluster1.markers, n = 5)
cluster0.markers <- FindMarkers(control01_cas9, ident.1 = 0, min.pct = 0.25)
head(cluster0.markers, n = 5)
```

## All markers

```{r message=FALSE, warning=FALSE}

control01_cas9.markers <- FindAllMarkers(control01_cas9, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
top_markers <- control01_cas9.markers %>% group_by(cluster) %>% top_n(n = 5, wt = avg_logFC)
DT::datatable(top_markers)
DT::datatable(control01_cas9.markers)

```

```{r message=FALSE, warning=FALSE}
VlnPlot(control01_cas9, features = c("Sftpc", "Hopx", "Foxj1","Scgb1c1", "Ager", "Aldh1a1", "Bpifb1", "Tppp3" ))

```

```{r message=FALSE, warning=FALSE}
top10 <- control01_cas9.markers %>% group_by(cluster) %>% top_n(n = 10, wt = avg_logFC)
DoHeatmap(control01_cas9, features = top10$gene) + NoLegend()

```

