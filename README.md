# Single-cell analysis of mouse lung tumorigenesis 


Study design: 

- Control samples:
  
  - control-tam: only tamoxifen induction for 2 weeks
  - control-cas9: samples are taken after adenovirus infection 

- Timepoints
  
  - 2 weeks after adenovirus infection 
  - 4 weeks after adenovirus infection
  - humane endpoint with tumor arising from green cells - only GFP+ soring
